================================================================================
TRABAJO DE PHP DE TECNOLOGÍAS DE DESARROLLO WEB
================================================================================
¿Qué acciones se han realizado para alcanzar el objetivo?
* En las peticiones POST he procesado los datos obteniendo 
  el Body de la petición.
* En las peticiones PUT, he procesado los datos obteniendo los paŕametros de
  la petición.
* En /group/groupname/:groupname había una condición según id.
  Eso creo que es incorrecto, así que lo he sustituido por groupname, que sería
  el parámetro adecuado.
* En POST /user, he utilizado el POST de /group como base, haciendo referencia
  a los repositorios correspondientes a la entity User. Lo extraño es el PHP
  Fatal Error que salta en phpunit, a pesar de mandar un 201 y el contenido del
  nuevo usuario.
* Por mención en clase, mencionado a su vez por un compañero, he modificado el
  fichero de pruebas de integración de grupos, quitando una asignación a null
  que carecía de sentido.
* En /user/username/:username tenía condición según id, 
  cambiada a según username.
