<?php require __DIR__ . '/header.php' ?>

<div id="message-bar" class="swagger-ui-wrap message-success"></div>
<div id="swagger-ui-container" class="swagger-ui-wrap"> 
  <h2>You have logged out.</h2>

  <p><a class="link_url" href="/">Back Home</a></p>
</div>

<?php require __DIR__ . '/footer.php' ?>